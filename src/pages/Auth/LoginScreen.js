import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Alert,
  TextInput,
  StyleSheet,
} from 'react-native';
import auth, {firebase} from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import { setUser } from '../../Redux/action';
import { useDispatch } from 'react-redux';
import Auth from '../../Service/Auth';

const LoginScreen = props => {
  const dispatch = useDispatch()

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const onLogin = () => {
    auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        // navigation.navigate('ChatScreen', {email: email});
        Alert.alert('Singin Success');
      })
      .catch(error => {
        Alert.alert('Error', error.code);
      });
  };

  const onLoginRDB = () => {
    try {
      database()
        // Pilih file users
        .ref('users/')
        .orderByChild('emailId')
        // sama dengan email
        .equalTo(email)
        // setiap data yang kita terima kita eksekusi
        .once('value')
        // Jika success maka
        .then(async snapshot => {
          // Jika value snapshot kosong
          if (snapshot.val() == null) {
            Alert.alert('Invalid Email Id');
            return false;
          }
          // user data diisi oleh object value snapshot
          let userData = Object.values(snapshot.val())[0];
          // Jika password tidak sama
          if (userData?.password != password) {
            Alert.alert('Error', 'Invalid Password!');
            return false;
          }
          console.log('User data: ', userData);
          dispatch(setUser(userData))
          await Auth.setAccount(userData)
          props.navigation.navigate('Home', {userData: userData});
        });
        // Jika error maka alert error
    } catch (error) {
      Alert.alert('Error', 'Not Found User');
    }
  };

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <View style={[styles.inputContainer, {marginTop: 10}]}>
        <TextInput
          style={styles.inputs}
          placeholder="Enter Your Email"
          onChangeText={value => {
            setEmail(value);
          }}
          value={email}
        />
      </View>
      <View style={[styles.inputContainer, {marginTop: 10}]}>
        <TextInput
          style={styles.inputs}
          placeholder="Enter Your Password"
          onChangeText={value => {
            setPassword(value);
          }}
          value={password}
          secureTextEntry={true}
        />
      </View>
      <TouchableOpacity style={styles.btn} onPress={() => onLoginRDB()}>
        <Text style={styles.btnText}>Login Now</Text>
      </TouchableOpacity>
      <View style={{flexDirection: 'row', marginTop: 10}}>
        <Text style={{color: '#000', marginRight: 5}}>
          Don't have an account?
        </Text>
        <TouchableOpacity onPress={() => props.navigation.navigate('RegisterScreen')}>
          <Text style={{color: '#b12441', fontWeight: 'bold'}}>
            Register Now
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  inputs: {
    borderBottomColor: 'black',
    color: 'black',
    paddingLeft: 10,
    flex: 1,
  },
  inputContainer: {
    borderRadius: 5,
    height: 48,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#f6f6f6',
    marginBottom: 10,
    borderColor: '#f6f6f6',
    borderWidth: 2,
    width: '90%',
  },
  btn: {
    backgroundColor: '#b12441',
    width: '90%',
    height: 50,
    borderRadius: 5,
    elevation: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    color: '#fff',
    fontSize: 14,
    marginTop: 2,
  },
});
