import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Alert,
  TextInput,
  StyleSheet,
} from 'react-native';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import uuid from 'react-native-uuid';

const RegisterScreen = props => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [name, setName] = useState('');
  const [about, setAbout] = useState('');

  const onRegister = async () => {
    setLoading(true);
    auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        setLoading(false);
        const userf = auth().currentUser;
        userf.updateProfile({displayName: name});
        Alert.alert('Success', `${name} Was Successful Created`);
      })
      .catch(error => {
        setLoading(false);
        Alert.alert('Oops', error.code);
      });
  };

  const onRegisterWithRDB = async () => {
    if (name === '' || email === '' || password === '' || about === '') {
      Alert.alert('Error', 'Harap isi Semua field');
      return false;
    }
    let data = {
      id: uuid.v4(),
      name: name,
      emailId: email,
      password: password,
      about: about,
      image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSQJxKGGpPc9-5g25KWwnsCCy9O_dlS4HWo5A&usqp=CAU'
    };
    try {
      alert(data.id);
      database()
        .ref('/users/' + data.id)
        .set(data)
        .then(() => {
          Alert.alert('Success', 'Register Successfully!');
          setEmail('');
          setPassword('');
          setName('');
          setAbout('')
          props.navigation.navigate('LoginScreen');
        });
    } catch (error) {
      Alert.alert('Error', error);
    }
  };

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <View style={[styles.inputContainer, {marginTop: 10}]}>
        <TextInput
          style={styles.inputs}
          placeholder="Enter New Email"
          onChangeText={value => {
            setEmail(value);
          }}
          value={email}
        />
      </View>
      <View style={[styles.inputContainer, {marginTop: 10}]}>
        <TextInput
          style={styles.inputs}
          placeholder="Enter New Password"
          onChangeText={value => {
            setPassword(value);
          }}
          value={password}
          secureTextEntry={true}
        />
      </View>
      <View style={[styles.inputContainer, {marginTop: 10}]}>
        <TextInput
          style={styles.inputs}
          placeholder="Enter Your Name"
          onChangeText={value => {
            setName(value);
          }}
          value={name}
        />
      </View>
      <View style={[styles.inputContainer, {marginTop: 10}]}>
        <TextInput
          style={styles.inputs}
          placeholder="Enter Your About"
          onChangeText={value => {
            setAbout(value);
          }}
          value={about}
        />
      </View>
      <TouchableOpacity style={styles.btn} onPress={onRegisterWithRDB}>
        <Text style={styles.btnText}>Register Now</Text>
      </TouchableOpacity>
      <View style={{flexDirection: 'row', marginTop: 10}}>
        <Text style={{color: '#000', marginRight: 5}}>
          You have an account?
        </Text>
        <TouchableOpacity
          onPress={() => props.navigation.navigate('LoginScreen')}>
          <Text style={{color: '#b12441', fontWeight: 'bold'}}>Login Now</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default RegisterScreen;

const styles = StyleSheet.create({
  inputs: {
    borderBottomColor: 'black',
    color: 'black',
    paddingLeft: 10,
    flex: 1,
  },
  inputContainer: {
    borderRadius: 5,
    height: 48,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#f6f6f6',
    marginBottom: 10,
    borderColor: '#f6f6f6',
    borderWidth: 2,
    width: '90%',
  },
  btn: {
    backgroundColor: '#b12441',
    width: '90%',
    height: 50,
    borderRadius: 5,
    elevation: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    color: '#fff',
    fontSize: 14,
    marginTop: 2,
  },
});
