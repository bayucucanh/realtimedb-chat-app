import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TextInput,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import ChatHeader from '../../components/Header/ChatHeader';
import MessageComp from '../../components/Chat/MessageComp';
import {bgChat} from '../../assets/images';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import moment from 'moment';
import {useSelector} from 'react-redux';
import database from '@react-native-firebase/database';

const SingleChat = props => {
  const user = useSelector(state => state.userData);

  const {receiverData} = props.route.params;

  // console.log('Receiver Data', receiverData);
  // console.log('user data: ', user);

  const [message, setmessage] = useState('');
  const [update, setUpdate] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const [allChat, setAllChat] = useState([]);

  useEffect(() => {
    const onChildAdd = database()
      .ref('/messages/' + receiverData.roomId)
      .on('child_added', snapshot => {
        // console.log('A new node has been added', snapshot.val());
        setAllChat(state => [snapshot.val(), ...state]);
      });

    // Stop listening for updates when no longer required
    return () =>
      database()
        .ref('/messages/' + +receiverData.roomId)
        .off('child_added', onChildAdd);
  }, [receiverData.roomId]);

  const msgvalid = txt => txt && txt.replace(/\s/g, '').length;

  const sendMessage = () => {
    if (message === '' || msgvalid(message) == 0) {
      alert('Enter something....');
      return false;
    }

    setDisabled(true)
    let messageData = {
      roomId: receiverData.roomId,
      message: message,
      from: user?.id,
      to: receiverData.id,
      sendTime: moment().format(),
      messageType: 'text',
    };

    const newReference = database()
      .ref('/messages/' + receiverData.roomId)
      .push();
    console.log('Auto generated key: ', newReference.key);
    messageData.id = newReference.key;
    newReference.set(messageData).then(() => {
      let chatListUpdate = {
        lastMessage: message,
        sendTime: messageData.sendTime,
      };

      database()
        .ref('/chatlist/' + receiverData?.id + '/' + user?.id)
        .update(chatListUpdate)
        .then(() => console.log('Data updated.'));

      database()
        .ref('/chatlist/' + user?.id + '/' + receiverData?.id)
        .update(chatListUpdate)
        .then(() => console.log('Data updated.'));

      setmessage('');
      setDisabled(false)
    });
  };

  const sorted = () => {
    return allChat.sort(function (a, b) {
      return new Date(b.sendTime) < new Date(a.sendTime)
        ? -1
        : new Date(b.sendTime) > new Date(a.sendTime)
        ? 1
        : 0;
    });
  };

  return (
    <View style={{position: 'relative', flex: 1}}>
      <ChatHeader data={receiverData} />
      <ImageBackground
        source={require('../../assets/images/bgChat.jpg')}
        style={{flex: 1}}>
        <FlatList
          style={{flex: 1}}
          data={allChat}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item, index) => index}
          inverted
          renderItem={({item}) => {
            return <MessageComp sender={item.from == user.id} item={item} />;
          }}
        />
      </ImageBackground>
      <View style={styles.msgWrapper}>
        <TextInput
          style={styles.msgInput}
          placeholder="type a messages"
          multiline={true}
          onChangeText={text => setmessage(text)}
          value={message}
        />
        <TouchableOpacity disabled={disabled} onPress={sendMessage}>
          <Icon name="send" size={25} color="#fff" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default SingleChat;

const styles = StyleSheet.create({
  msgWrapper: {
    backgroundColor: '#0a2141',
    elevation: 2,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 7,
    justifyContent: 'space-evenly',
  },
  msgInput: {
    backgroundColor: '#fff',
    width: '80%',
    borderRadius: 16,
    borderWidth: 0.5,
    borderColor: '#fff',
    paddingHorizontal: 15,
    color: '#000',
  },
});
