import {StyleSheet, Text, View, Image, TouchableOpacity, FlatList} from 'react-native';
import React, {useState, useEffect} from 'react';
import {useSelector} from 'react-redux';
import HomeHeader from '../../components/Header/HomeHeader';
import AllUser from '../User/AllUser';
import database from '@react-native-firebase/database';

const Home = props => {
  const user = useSelector(state => state.userData);

  const [chatList, setChatList] = useState([]);

  useEffect(() => {
    getChatList();
  }, []);

  const getChatList = async () => {
    database()
      .ref('/chatlist/'+user.id)
      .on('value', snapshot => {
        // console.log('User data: ', Object.values(snapshot.val()));
        if (snapshot.val() != null) {
          setChatList(Object.values(snapshot.val()))
        }
      });
  };

  const renderItem = ({item}) => (
    <TouchableOpacity style={styles.wrapper} onPress={()=> props.navigation.navigate('SingleChat', {receiverData:item})}>
      <Image source={{uri: item.image}} style={styles.imageProfile} />
      <View>
        <Text style={styles.nameUser}>{item.name}</Text>
        <Text>{item.lastMessage}</Text>
      </View>
    </TouchableOpacity>
  );

  return (
    <View style={styles.container}>
      <HomeHeader image={user.image} />
      <FlatList
        showsVerticalScrollIndicator={false}
        keyExtractor={(item, index) => index.toString()}
        data={chatList}
        renderItem={renderItem}
      />
      <TouchableOpacity style={styles.buttonContact} onPress={()=> props.navigation.navigate('AllUser')}>
        <Text>+</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f9fafe',
  },
  wrapper: {
    marginHorizontal: 12,
    marginTop: 14,
    flexDirection: 'row',
    backgroundColor: '#fff',
    padding: 10,
  },
  imageProfile: {
    height: 60,
    width: 60,
    borderRadius: 30,
    marginRight: 15,
  },
  nameUser: {
    fontSize: 16,
    fontWeight: '600',
    color: '#000',
    marginTop: 4,
  },
  buttonContact: {
    position: 'absolute',
    bottom: 15,
    right: 15,
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: 'yellow',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 5,
  },
});
