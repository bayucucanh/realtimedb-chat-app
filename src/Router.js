import React, {useEffect, useState} from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
// import { Analytics, LoginScreen, RegisterScreen } from './pages';
import {RegisterScreen, LoginScreen} from './pages/Auth';
import Home from './pages/Home';
import Auth from './Service/Auth';
import {setUser} from './Redux/action';
import {useDispatch, useSelector} from 'react-redux';
import SingleChat from './pages/Home/SingleChat';
import AllUser from './pages/User/AllUser';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const AuthStack = () => {
  return (
    <Stack.Navigator initialRouteName="LoginScreen">
      <Stack.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="RegisterScreen"
        component={RegisterScreen}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

const AppStack = () => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="AllUser"
        component={AllUser}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="SingleChat"
        component={SingleChat}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  )
}

const Router = () => {
  const dispatch = useDispatch();

  const [loginCheck, setLoginCheck] = useState(true)

  const user = useSelector(state => state.userData);
  const login = useSelector(state => state.isLogin);

  useEffect(() => {
    getUser();
  }, []);

  const getUser = async () => {
    // Mengambil data yang ada pada storage
    let data = await Auth.getAccount();
    if (data != null) {
      // mengirim data yang ada pada storage ke action setUser
      dispatch(setUser(data));
      // pengecekan login di buat false
      setLoginCheck(false)
    } else {
      // pengecekan login di buat false
      setLoginCheck(false)
    }
  };

  // digunakan agar langsung menampilkan halaman home
  if (loginCheck) {
    // ganti loading
    return null
  }

  return (
    <Stack.Navigator initialRouteName="AuthStack">
      {!login ? (
        <Stack.Screen
          name="AuthStack"
          component={AuthStack}
          options={{headerShown: false}}
        />
      ) : (
        <Stack.Screen
          name="AppStack"
          component={AppStack}
          options={{headerShown: false}}
        />
      )}
    </Stack.Navigator>
  );
};

export default Router;
