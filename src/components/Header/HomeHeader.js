import {StyleSheet, Text, View, Image} from 'react-native';
import React from 'react';
import { useSelector } from 'react-redux';

const HomeHeader = ({image}) => {
  console.log('image', image);
  return (
    <View style={styles.container}>
      <Text style={styles.chatsLogo}>Chats</Text>
      <View style={{flexDirection:'row',alignItems:'center'}}>
        {/* <Text>{name}</Text> */}
        <Image
          style={styles.avatar}
          source={{
            uri: image,
          }}
        />
      </View>
    </View>
  );
};

export default HomeHeader;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
    paddingHorizontal: 15,
    paddingVertical: 15,
    backgroundColor: '#0a2141',
    elevation: 2,
  },
  chatsLogo: {
    fontSize: 22,
    fontWeight: 'bold',
    color: '#fff'
  },
  avatar: {
    height: 40,
    width: 40,
    borderRadius: 40 /2
  }
});
