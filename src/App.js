import React, {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import Router from './Router';
import {Provider} from 'react-redux';
// import {PersistGate} from 'redux-persist/integration/react';
import { Store } from './Redux/store';

function App() {

  return (
    <Provider store={Store}>
      <NavigationContainer>
        <Router />
      </NavigationContainer>
    </Provider>
  );
}

export default App;
